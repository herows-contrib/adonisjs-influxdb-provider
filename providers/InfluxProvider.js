'use strict'

/**
 *
 * adonis-influxdb
 *
 * (c) HeroGuest <dev@heroguest.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
const { ServiceProvider } = require('@adonisjs/fold')

/*
 * Exposes the Influx connection to the
 */
class InfluxProvider extends ServiceProvider {
  register () {
    this.app.bind('Adonis/Addons/InfluxFactory', () => require('../src/InfluxFactory'))

    this.app.singleton('Adonis/Addons/Influx', (app) => {
      const InfluxFactory = app.use('Adonis/Addons/InfluxFactory')
      const Config = app.use('Adonis/Src/Config')
      const Influx = require('../src/Influx')
      return new Influx(Config, InfluxFactory)
    })

    this.app.alias('Adonis/Addons/Influx', 'Influx')
  }
}

module.exports = InfluxProvider
