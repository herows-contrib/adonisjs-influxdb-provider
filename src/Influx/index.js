'use strict'

/**
 *
 * adonis-influxdb
 *
 * (c) HeroGuest <dev@heroguest.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
const GE = require('@adonisjs/generic-exceptions')
const _ = require('lodash')
const proxyHandler = require('./proxyHandler')

/**
 * Influx class is used to call methods on a influxdb server.
 *
 * @namespace Adonis/Addons/Influx
 * @singleton
 * @alias Influx
 *
 * @class Influx
 * @constructor
 */
class Influx {
  constructor (Config, Factory) {
    this.Config = Config
    this.Factory = Factory
    this.connectionPools = {}
    return new Proxy(this, proxyHandler)
  }

  /**
   * Returns instance of a new factory instance for
   * a given connection.
   *
   * @param  {String} [connection='']
   *
   * @return {RedisFactory}
   */
  connection (connection = '') {
    connection = connection || this.Config.get('influx.connection')
    const config = this.Config.get(`influx.${connection}`)

    return this.namedConnection(connection, config)
  }

  /**
   * Looks at the config file and tells if pool is configured
   *
   * @param {Object} config
   *
   * @return {Boolean}
   *
   * @private
   */
  _isPool (config) {
    return !!config.usePool
  }

  /**
   * Creates a connection using raw config and adds it to the
   * connection pool.
   *
   * @method namedConnection
   *
   * @param  {String}        name
   * @param  {Object}        config
   *
   * @return {RedisFactory}
   */
  namedConnection (name, config) {
    if (this.connectionPools[name]) {
      return this.connectionPools[name]
    }

    if (!config || !_.size(config) === 0) {
      throw GE.RuntimeException.missingConfig(name || 'configuration for influxdb', 'config/influx.js')
    }

    this.connectionPools[name] = new this.Factory(config, this._isPool(config))
    return this.connectionPools[name]
  }

  /**
   * Returns a hash of connection pools
   *
   * @return {Object}
   *
   * @public
   */
  getConnections () {
    return this.connectionPools
  }
}

module.exports = Influx
