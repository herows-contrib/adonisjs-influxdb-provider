'use strict'

/**
 *
 * adonis-influxdb
 *
 * (c) HeroGuest <dev@heroguest.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
const influxdb = require('influx')
const debug = require('debug')('adonis:influx')
const proxyHandler = require('./proxyHandler')

class InfluxFactory {
  constructor (config, usePool = false) {
    this._config = config
    this._usePool = usePool

    /**
     * Main influxdb connection
     *
     * @attribute connection
     *
     * @type {Object}
     */
    this.connection = null

    /*
     * Connect to InfluxDB
     */
    this.connect()
    return new Proxy(this, proxyHandler)
  }

  /**
   * Creates a new Influx connection
   *
   * @method _newConnection
   *
   * @return {Object}
   */
  _newConnection () {
    if (this._usePool) {
      throw new Error('usePool not implemented yet')
    }

    debug('creating new influxdb connection using config: %s', this._config)
    return new influxdb.InfluxDB(this._config)
  }

  connect () {
    this.connection = this._newConnection()
  }
}

module.exports = InfluxFactory
