'use strict'

/**
 *
 * adonis-influxdb
 *
 * (c) HeroGuest <dev@heroguest.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

const test = require('japa')
const { Config } = require('@adonisjs/sink')
const Influx = require('../src/Influx')
const InfluxFactory = require('../src/InfluxFactory')

test.group('InfluxDB', () => {
  test('should throw exception when connection is not defined in influx config file', (assert) => {
    const connection = new Influx(new Config(), InfluxFactory)
    const throwingFn = () => connection._getConfig()
    assert.throw(throwingFn, /^E_MISSING_CONFIG: configuration for influx/)
  })

  test('should return the instance of influx factory when using connection method', (assert) => {
    const config = new Config()
    config.set('influx', {
      connection: 'primary',
      primary: { host: 'influxdb.test', database: 'testingdb' }
    })

    const connection = new Influx(config, InfluxFactory)
    assert.equal(connection.connection() instanceof InfluxFactory, true)
  })

  test('should proxy InfluxDB methods', (assert) => {
    const config = new Config()
    config.set('influx', {
      connection: 'primary',
      primary: { host: 'influxdb.test', database: 'testingdb' }
    })

    const influx = new Influx(config, InfluxFactory)
    influx.connect()

    const writePoints = influx.writePoints
    assert.isFunction(writePoints, 'function')

    const ping = influx.ping
    assert.isFunction(ping, 'function')

    const schema = influx.schema
    assert.isObject(schema, 'object')
  })

  test('should create new connections and add them to the pool', (assert) => {
    const config = new Config()
    config.set('influx', {
      connection: 'primary',
      primary: { host: 'influxdb.test', database: 'testingdb' }
    })

    const influx = new Influx(config, InfluxFactory)
    influx.connection()

    const rawConfig = { host: 'influxdb.test', database: 'testingdb' }
    influx.namedConnection('secondary', rawConfig)
    assert.hasAllKeys(influx.getConnections(), ['primary', 'secondary'])
  })

  test('should throw exception when trying to use pools (not supported)', (assert) => {
    const config = new Config()
    config.set('influx', {
      connection: 'primary',
      primary: { host: 'influxdb.test', database: 'testingdb', usePool: true }
    })

    const throwingFn = () => {
      const influx = new Influx(config, InfluxFactory)
      influx.connection()
    }
    assert.throw(throwingFn, /usePool not implemented yet/)
  })
})
